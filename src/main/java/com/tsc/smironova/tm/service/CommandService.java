package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.ICommandRepository;
import com.tsc.smironova.tm.api.ICommandService;
import com.tsc.smironova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}

package com.tsc.smironova.tm.boostrap;

import com.tsc.smironova.tm.api.ICommandController;
import com.tsc.smironova.tm.api.ICommandRepository;
import com.tsc.smironova.tm.api.ICommandService;
import com.tsc.smironova.tm.constant.ArgumentConst;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.controller.CommandController;
import com.tsc.smironova.tm.repository.CommandRepository;
import com.tsc.smironova.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(this.commandRepository);
    private final ICommandController commandController = new CommandController(this.commandService);

    public void run(final String... args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        parseArgs(args);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseCommand(final String command) {
        if (command == null)
            return;
        switch (command) {
            case TerminalConstant.ABOUT -> this.commandController.showAbout();
            case TerminalConstant.VERSION -> this.commandController.showVersion();
            case TerminalConstant.HELP -> this.commandController.showHelp();
            case TerminalConstant.INFO -> this.commandController.showInfo();
            case TerminalConstant.COMMANDS -> this.commandController.showCommands();
            case TerminalConstant.ARGUMENTS -> this.commandController.showArguments();
            case TerminalConstant.EXIT -> this.commandController.exit();
            default -> this.commandController.showIncorrectArgument();
        }
    }

    public void parseArg(final String arg) {
        if (arg == null)
            return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT -> this.commandController.showAbout();
            case ArgumentConst.ARG_VERSION -> this.commandController.showVersion();
            case ArgumentConst.ARG_HELP -> this.commandController.showHelp();
            case ArgumentConst.ARG_INFO ->  this.commandController.showInfo();
            default -> this.commandController.showIncorrectCommand();
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        final String arg = args[0];
        parseArg(arg);
        this.commandController.exit();
    }

}
